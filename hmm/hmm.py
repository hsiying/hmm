# Author : Awesome Tomato <awesome.tomato.digital@gmail.com>


from functools import reduce 
import numpy as np

class HMMMultinomial():
    """
    Hidden Markov Model:
    
    This module contains:
    - filtering function
    - hidden state prediction
    - future observation prediction 
    - smoothing function
    - backward propagation function 
    - forwardbackward function
    - viterbi function
    """
    
    def __init__(self, px0, theta, phi, y):
        """
        px0: 1-D array or list, 
        theta: n by n transition matrix 
        phi: observation matrix 
        y: observation vector 
        """
        self.px0 = np.array(px0)
        self.theta = np.array(theta)
        self.phi = np.array(phi)
        self.nstate = theta.shape[0]
        self.y = np.array(y)
        self.t = len(y)
        
        if self.px0.shape[0]<2:
            raise ValueError

        if self.theta.shape[0] != self.phi.shape[0]:
            raise ValueError 
 
    
    def filtering(px0 = self.px0, 
                  theta = self.theta, 
                  phi= self.theta, 
                  y= self.y):
        """
        infer P(Xt| Y1:t)
        """

        #  for t = 0
        yt = y[0]
        p_prior = normalize(np.multiply(phi[:, yt], px0))
        #   for t > 0
        for t in range(1, len(y)):
            print("-"*25 + " t = ", t, "-"*25)
            yt = y[t]          
            p_pred = np.multiply(theta.transpose(), p_prior).sum(axis=1)            
            p_obs = phi[:, yt]
            p_prior = normalize(np.multiply(p_pred, p_obs))
            
        return p_prior
    
    def hidden_state_prediction(h, px,theta = self.theta):
        """
        px: is returned from the filtering function. P(Xt|Y_1:t), vector
        h: int. future steps
        theta: transition matrix 
        return: hidden state probablity vector P(X_t+h| Y1:t)
        """
        def px_pred(px, theta):
            return np.multiply(theta.transpose(), px).sum(axis=1)

        if h == 1:
            return px_pred(px, theta)
        return hidden_state_prediction(h-1,theta, px_pred(px, theta))
    
    def future_observation_prediction(phi, px, y = None):
        """
        predict h step obseration. note that the px here is h steps ahead
        y: int observation category 
        phi: local evidence matrix 
        px: P(X_t+h| Y_1:t)
        default return all the observational types 
        return observation probability vector P(Yt+h| Y_1:t)
        """
        y = range(phi.shape[1]) if y is None else y
        p_obs = np.array([ phi[i]*px[i] for i in range(len(px))]).sum(axis=0)
        return p_obs[y]
    
    def _forward_walk( phi, theta, y, px, t, px_list= None):
        """
        calculate posterior prob
        return the track of posterior probs given history of observation
        """

        if  px_list is None:
            px_list=[]

        def posterior(px, t ,phi, theta, y): 
            y = np.flip(y)
            yt = y[t]
            p_pred = np.multiply(theta.transpose(), px).sum(axis =1)
#             px = np.multiply(p_pred, phi[:, yt])/ np.multiply(p_pred, phi[:, yt]).sum()
            px = normalize(np.multiply(p_pred, phi[:, yt]))
            return px 

        px = np.array(px)
        px_list.append(px)
        if t == 0:
            px_list.append(posterior(px = px, phi= phi, theta = theta, y=y, t = t) )
            return  px_list
        return forward_walk(phi, theta, y, posterior(px= px,t= t ,phi=phi, theta=theta, y=y), t-1, px_list)
    
    
    def forward_propagation(px0, y, theta, phi):
        """
        px0 :1-D list or array, initial probablity distribution 
        y: observation 1-D array
        theta: transition matrix 
        phi: local evidence matrix 
        return: prob matrix for each state at time t given previous and current observations P(Xt| Y1:t)
        """
        theta = np.array(theta)
        phi = np.array(phi)
        px0 = np.array(px0)
        t = len(y)- 2

        # init  prob 
        yt = y[0]
#         p_prior = np.multiply(phi[:, yt], px0)/sum(np.multiply(phi[:, yt], px0))
        p_prior = normalize(np.multiply(phi[:, yt], px0))
        px_list = list(p_prior) 

        return _forward_walk(phi, theta, y, px = p_prior, t = t, px_list=px_list)
    
    def _backward_walk(t, theta, phi, y, p_T, p_list= None):
        if p_list is None:
            p_list = []

        yt = y[t-1]
        phi_yt = phi.transpose()[yt]

        p_T = reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1)
        print('-'*25+ 't = ', str(t)+ '-'*25 +'\ny = '+str(yt))
        print('p_T = '+str(p_T))
        p_list.append(p_T)
        if t == 1:
    #         p_list.append(p_T)
            return p_list

        return backward_walk(t-1, theta, phi, y, p_T, p_list = p_list)
    
    def _backward_walk_normalized(t, theta, phi, y, p_T, p_list = None):
        if p_list is None:
            p_list = []

        yt = y[t-1]
        phi_yt = phi.transpose()[yt]
#         p_T = reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1)/reduce(np.multiply, (theta, phi_yt, p_T)).sum()
        p_T = normalize(reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1))
        p_list.append(p_T)
        print('-'*25+ 't = ', str(t)+ '-'*25 +'\ny = '+str(yt))
        print('p_T = '+str(p_T))

        if t == 1:
            return p_list

        return backward_walk_normalized(t-1, theta, phi, y, p_T, p_list= p_list)

    def backward_propagation(theta, phi, y, normalized = False):
        """
        P(Xt+1:T|yT)
        theta: transition matrix 
        phi: local evidence matrix 
        y: observations
        return: matrix for each observation at time t P(Xt+1:T|yT)
        """
        n_state = theta.shape[0]
        p_T = np.array([1]* n_state)
        p_list = p_T
        t = len(y)
        if normalized:
            return _backward_walk_normalized(t, theta, phi, y, p_T)
        else:
            return _backward_walk(t, theta, phi, y, p_T)
        
    
    def viterbi(px0, theta, phi, y):
        """
        px0: initial state prob distribution
        theta: transition matrix
        phi: transmission prob matrix or local evidence 
        y: observed sequence
        return the most likely hidden state
        """
        # make sure except for y all matrix and vectors are betwn 0 and 1 
        # ... check type must be float must be np.array
        # initiating the 1st step 
        # s is MAP
        # warning: ignore the divide by 0 encountered in log calc
        np.seterr(divide = 'print')

        px0 = np.array(px0).astype('float')
        theta = theta.astype('float')
        phi = phi.astype('float')
        y = y.astype('int')
        t = 0 
        yt = y[t]
        s = []

        log_p = np.log(phi[:,yt]) + np.log(px0)
        delta = np.amax(log_p)
        s.append(np.argmax(log_p))

        for t in range(1, len(y)):     
            yt = y[t]
            st = s[t-1]
            log_p = np.log(theta[st])+ np.log(phi[:, yt]) + delta
            delta = np.amax(log_p)
            s.append(np.argmax(log_p))
        return s 



