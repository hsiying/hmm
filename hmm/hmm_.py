
from functools import reduce 
import numpy as np

def normalize(x):
    """
    x: np.array 
    """
    if type(x) is not np.ndarray:
        raise TypeError('numpy.ndarray is required. It appears that x is {}'.format(type(x)))
    return x / sum(x)


def normalize_mtx(x , dim = 3):
#     assert type(x) == 'numpy.ndarray'
#     assert to be float
    x = np.array(x)  
    if dim == 1:
        return x/x.sum(axis = 0)
    if dim == 2:
        return x/x.sum(axis = 1)
    if dim == 3:
        return x/x.flatten().sum()


def filtering(px0, theta, phi, y, verbose = True):
    # set up
    theta = np.array(theta)
    phi = np.array(phi)
    px0 = np.array(px0)

    #  for t = 0
    yt = y[0]
    p_prior = normalize(np.multiply(phi[:, yt], px0))
    if verbose:
        print("-"*25+ " t = 1 initiation " + "-"*25)
        print("P(X1|Y1) = {}, y1 = {}".format(p_prior, yt))
    # for t > 0
    for t in range(1, len(y)):
        yt = y[t] 
        p_pred = np.multiply(theta.transpose(), p_prior).sum(axis=1)   
        p_obs = phi[:, yt]
        p_posterior = normalize(np.multiply(p_pred, p_obs))
        if verbose:
            # i is normal time step count, t is python step count
            i = t + 1
            print("-"*25 + " t = ", i, "-"*25)
            print("P(X{}|Y{})= {} , y{} = {} ".format(i-1, i-1,p_prior,i, yt))
            print("P(X{}|Y1:{}) = {}".format(i, i-1, p_pred))
            print("P(Y{}|X{}) = {}".format(i, i, p_obs))
            print("P(X{}|Y1:{})= {}".format(i, i, p_posterior))       
        p_prior = p_posterior 
    return p_prior

def forward_walk( phi, theta, y, px, t, px_list= None , verbose = True):
    """
    calculate posterior prob
    return the track of posterior probs given history of observation
    """
    
    if  px_list is None:
        px_list=[]
    
    def posterior(px, t ,phi, theta, y): 
        y = np.flip(y)
        yt = y[t]
        p_pred = np.multiply(theta.transpose(), px).sum(axis =1)
        # px = np.multiply(p_pred, phi[:, yt])/ np.multiply(p_pred, phi[:, yt]).sum()
        px = normalize(np.multiply(p_pred, phi[:, yt]))
        return px 
    
    if px is not np.array:
        px = np.array(px)
        
    px_list.append(px)
    
    if verbose:
        i = len(y) - t - 1
        print("P(X{}|Y1:{}): {}".format(i,i,px))
    
    if t == 0:
        px_list.append(posterior(px = px, phi= phi, theta = theta, y=y, t = t) )
        return  px_list
    
    return forward_walk(phi, theta, y, posterior(px = px,t= t ,phi=phi, theta=theta, y=y), t-1, px_list, verbose = verbose)

def forward_propagation(px0, y, theta, phi, verbose = True):
    """
    px0 :1-D list or array, initial probablity distribution 
    y: observation 1-D array
    theta: transition matrix 
    phi: local evidence matrix 
    return: prob matrix for each state at time t given previous and current observations P(Xt| Y1:t)
    """
    theta = np.array(theta)
    phi = np.array(phi)
    px0 = np.array(px0)
    t = len(y)- 2
    
    # init  prob 
    yt = y[0]
    # p_prior = np.multiply(phi[:, yt], px0)/sum(np.multiply(phi[:, yt], px0))
    p_prior = normalize(np.multiply(phi[:, yt], px0))
    px_list = []
    if verbose:
        print('-'*25 + ' initiating forward propogation ' + '-'*25)   
    return forward_walk(phi, theta, y, px = p_prior, t = t, px_list=px_list, verbose = verbose)

def hidden_state_prediction(theta, px, h):
    """
    P(Xt+h|Y1:t)
    px is returned from the filtering function.
    
    """
    def px_pred(px, theta):
        return np.multiply(theta.transpose(), px).sum(axis=1)

    if h == 1:
        return px_pred(px, theta)
    
    return hidden_state_prediction(theta, px_pred(px, theta), h-1)

def future_observation_prediction(phi, px, y = None):
    """
    """
    y = range(phi.shape[1]) if y is None else y
    p_obs = np.array([ phi[i]*px[i] for i in range(len(px))]).sum(axis=0)
    return p_obs[y]

def backward_walk(t, theta, phi, y, p_T, p_list= None, verbose = True):
    if p_list is None:
        p_list = []
        
    yt = y[t-1]
    phi_yt = phi.transpose()[yt]
    
    p_T = reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1)
    if verbose:
        print("P(Y{}:{}|X{}):{}".format(t+1, len(y), t, p_T))
    p_list.append(p_T)
    if t == 1:
    # p_list.append(p_T)
        p_list.reverse()
        return p_list
    
    return backward_walk(t-1, theta, phi, y, p_T, p_list = p_list, verbose = verbose)

def backward_walk_normalized(t, theta, phi, y, p_T, p_list = None, verbose = True):
    if p_list is None:
        p_list = []
        
    yt = y[t-1]
    phi_yt = phi.transpose()[yt]
    # p_T = reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1)/reduce(np.multiply, (theta, phi_yt, p_T)).sum()
    p_T = normalize(reduce(np.multiply, (theta, phi_yt, p_T)).sum(axis=1))
    p_list.append(p_T)
    if verbose:
        print("P(Y{}:{}|X{}):{}".format(t+1, len(y), t, p_T))
    
    if t == 1:
        p_list.reverse()
        return p_list
    
    return backward_walk_normalized(t-1, theta, phi, y, p_T, p_list= p_list, verbose = verbose)


def backward_propagation(theta, phi, y, normalized = False, verbose = True):
    """
    P(Xt+1:T|yT)
    theta: transition matrix 
    phi: local evidence matrix 
    y: observations
    """
    n_state = theta.shape[0]
    p_T = np.array([1]* n_state)
    p_list = p_T
    t = len(y)
    if verbose:
        print('-'*25 +" starting backward propagation "+ '-'*25)
    if normalized:
        if verbose:
            print("Use normalized posterior in the backward progagation.")
        return backward_walk_normalized(t, theta, phi, y, p_T, verbose = verbose)
    else:
        return backward_walk(t, theta, phi, y, p_T, verbose = verbose)
    
def foward_backward(forward, backward):
    """
    forward backward algorithm:
    forward: step-state prob matrix P(Xt|Y1:t)
    backward: P(Yt+1:T|Xt)
    return: step- state prob matrix
    """
    fw = np.array(forward)
    bw = np.array(backward)
    shape_f = fw.shape
    shape_b = bw.shape
    
    if shape_f == shape_b:
        return np.multiply(fw, bw)
    else:
        raise TypeError

def viterbi(px0, theta, phi, y):
    """
     px0: initial state prob distribution
        theta: transition matrix
        phi: transmission prob matrix or local evidence 
        y: observed sequence
        return the most likely hidden state
    """
    # make sure except for y all matrix and vectors are betwn 0 and 1 
    # ... check type must be float must be np.array
    # initiating the 1st step 
    # s is MAP
    # warning: ignore the divide by 0 encountered in log calc
    np.seterr(divide = 'print')
    
    px0 = np.array(px0).astype('float')
    theta = theta.astype('float')
    phi = phi.astype('float')
    y = y.astype('int')
    t = 0 
    yt = y[t]
    s = []
    
    log_p = np.log(phi[:,yt]) + np.log(px0)
    delta = np.amax(log_p)
    s.append(np.argmax(log_p))
    
    for t in range(1, len(y)):     
        yt = y[t]
        st = s[t-1]
        log_p = np.log(theta[st])+ np.log(phi[:, yt]) + delta
        delta = np.amax(log_p)
        s.append(np.argmax(log_p))
    return s 

def two_slice_prod(alpha, beta, theta, phi, normalize=True, verbose= True):
    """
    matrix multiplications for two-slice margin
    alpha: n x 1 vector P(Xt| Y1:t)
    beta: n x 1 vector P(Xt+1| Y1:T)
    theta: n x n transition matrix P(Xt+1|Xt)
    phi: n x 1 emission matrix , P(Yt|Xt)
    n: number of states
    """
    assert alpha.shape[0] == beta.shape[0] == theta.shape[0] == phi.shape[0]
    n = theta.shape[0]
    phi = np.tile(phi, (n, 1))
    c = np.tensordot(alpha, beta, axes = 0)
    if verbose:
        print('alpha:{}; beta:{};'.format(alpha, beta))
    if normalize:
        return normalize_mtx(reduce(np.multiply, [c, theta, phi]))
    else:
        return reduce(np.multiply, [c, theta, phi])

def smoothing(t, alpha, beta, phi, theta, y,normalize, verbose):
    a = alpha[t]
    b = beta[t+1]
    yt= y[t+1]
    f = phi[:, yt]
    return two_slice_prod(a, b, theta, f, normalize = normalize, verbose = verbose)    

def two_slice_smoothing(px0, theta, phi, y, normalize = True, verbose = True):
    """
    compute expected number of transitions from state i to state j
    N_ij = sum(t=1, t = T-1)P(Zt=i, Zt+1= j|X1:T)
    px0: vector init prob of each states
    theta: transition matrix 
    phi: class conditional density 
    y: observtion 
    normalize: boolean if true normalize the output probs
    """
    # forward
    alpha = forward_propagation(px0, y, theta, phi, verbose= verbose)
    # backward
    beta = backward_propagation(theta, phi, y, normalized = True, verbose = verbose)
    
    if verbose:
        print('-'*25 + ' starting two-slice smoothing ' +'-'*25)
    if normalize:
        print("** Use normalized posterior to update transition matrix **")
    return [smoothing(t, alpha, beta, phi, theta, y,normalize=normalize, verbose = verbose) for t in range(len(y)-1)]

def fwdbwd(px0, theta, phi, y, verbose = True):
    """
    foward backward algorigthm
    return: gmma: prob of each state and full observations at node P(Xt|Y1:T)
    """
    fwd = forward_propagation(px0, y, theta, phi, verbose= verbose)
    bwd = backward_propagation(theta, phi, y, normalized= True, verbose= verbose)
    fwd = np.array(fwd)
    bwd = np.array(bwd)
    if fwd.shape != bwd.shape:
        raise ValueError("wrong dimension ... ")
    return np.multiply(fwd, bwd)
    

def e_px(px, y, phi, verbose = True):
    """
    initiation prob
    """
    yt = y[0]
    px_est = normalize(np.multiply(phi[:, yt], px))
    if verbose:
        print('**** p(Xt-1)= {}****\n**** Estimated p(Xt)= {} ****'.format(px, px_est))
    return px_est

def e_theta(px, theta, phi,y, verbose = True):
    """
    estimate transition matrix
    """
    a = two_slice_smoothing(px, theta, phi, y, normalize= False, verbose= verbose)
    a_T = reduce(lambda x, y: x +y, a)
    if verbose:
        print("**** theta(t-1): *****\n{}".format(theta))
        print('**** Before normalization the matrix: ****\n{}'.format(a_T))
    return [normalize(i) for i in a_T]

def em_phi(px, theta, phi, Y, verbose = True):
    """
    esstimate transmission matrix 
    """
    gamma = [fwdbwd(px, theta, phi, i , verbose= verbose) for i in Y]
    val = [ j for i in gamma for j in i]
    ys = [j for i in Y for j in i]
    d = pd.DataFrame(val, index = ys)
    joint_xy = d.groupby(d.index).sum().values
    margin = d.sum(axis = 0).values
    phi_t = joint_xy/margin
    return phi_t.transpose()

def em_update(px0, theta0, phi0, Y, verbose= True):
    """
    estimation step:
    feeding with multiple observation sequences: Y is a matrix supposed to be symetric?
    initation parameters:
        - p0: prob of each state
        - theta0: transition matrix
        - phi0: transmission matrix
    """
    N = len(Y)
    # initiation prob 
    px_i= [e_px(px0, i, phi0, verbose= verbose) for i in Y]
    px = np.divide(np.array(px_i).sum(axis = 0), N) # return prob of each state
    
    # transition matrix
    theta_i = [e_theta(px0, theta0, phi0, i, verbose= verbose) for i in Y]
    theta = np.array(theta_i).sum(axis=0)/ np.array(theta_i).sum(axis=0).sum(axis= 1)
    # transmission matrix
    phi = em_phi(px0, theta0, phi0, Y, verbose= verbose)
    
    return px, theta, phi