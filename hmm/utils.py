
def normalize(x):
    """
    x: np.array 
    """
    if type(x) is not np.ndarray:
        raise TypeError('numpy.ndarray is required. It appears that x is {}'.format(type(x)))
    return x / sum(x)

def withinrange(x, lb, ub):
    return True if x >= lb and x <= ub else False 

def normalize_mtx(x):
#     assert type(x) == 'numpy.ndarray'
#     assert to be float
    x = np.array(x)    
    return x/x.flatten().sum()

def find_all_obs(Y):
    """
    Y: n-d list contains observations 
    """
    return list(set([j  for i in Y for j in i]))
