from setuptools import setup, find_packages
from version import find_version
setup(
    name = 'markov_chain',
    author = 'Awesome tomato',
    description = 'mm, hmm',
    license = 'MIT',
    packages = find_packages(),
    version = find_version('markov_chain','__init__.py')
    
)