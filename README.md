# Markov Chain and Hidden Markov Model
This is a project to learn Markov models 

## Markov Chain
Explained in 5 sentences.


## Hidden Markov Chain 
Explained in 6 sentences. 

**The most frequent updates:**  

:fire: `:fire:` Removing code or files.  
:bug: `:bug:` Fixing a bug.  
:sparkles: `:sparkles:` Introducing new features.  
:lipstick: `:lipstick:` Updating the UI and style files.   
:pencil: `:pencil:` Writing docs.  
:construction: `:construction:` Work in progress.  


**second common commits:**    

:art: `:art:` Improving structure / format of the code.  
:zap: `:zap:` Improving performance.  
:white_check_mark: `:white_check_mark:` Updating tests.  
:green_heart: `:green_heart:` Fixing CI Build.  
:tada: `:tada:` Initial commit.  
:recycle: `:recycle:` Refactoring code.  
:pencil2: `:pencil2:` Fixing typos.  
:poop: `:poop:` Writing bad code that needs to be improved.  
:bulb: `:bulb:` Documenting source code.  
:loud_sound: `:loud_sound:` Adding logs.  
:mute: `:mute:` Removing logs.  
:speech_balloon: `:speech_balloon:` Updating text and literals.  
:card_file_box: `:card_file_box:` Performing database related changes.  
:see_no_evil: `:see_no_evil:` Adding or updating a .gitignore file  
:camera_flash: `:camera_flash:` Adding or updating snapshots  
:truck: `:truck:` Moving or renaming files.  

